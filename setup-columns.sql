alter table appconfig
    alter column authors type text using authors::text,
    alter column cameras type text using cameras::text,
    alter column main_title type text using main_title::text,
    alter column sub_title type text using sub_title::text;

alter table collections
    alter column name type text using name::text,
    alter column structure type text using structure::text,
    alter column imagecontent type text using imagecontent::text,
    alter column textcontent type text using textcontent::text,
    alter column description type text using description::text,
    alter column carouselcontent type text using carouselcontent::text;

alter table images
    alter column author type text using author::text,
    alter column camera type text using camera::text,
    alter column description type text using description::text,
    alter column name type text using name::text,
    alter column tags type text using tags::text,
    alter column title type text using title::text;

alter table maps
    alter column referenced_images type text using referenced_images::text,
    alter column name type text using name::text;
