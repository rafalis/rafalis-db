# Rafalis Database

Rafalis uses a dockerised PostgreSQL database, listening on **port 5432**.

## Dev mode

To launch the database in development mode : 

```bash
$ docker-compose up -d
```

This will initiate a database if it doesn't already exists, and the Rafalis server will take care of creating the scheme etc.

The database will be located in a `data/` directory.

When initiating a new database (after the first server launch that will initiate the database schema), run the following to correctly configure the column types (this is a one time run script, no need to run it at each database launch). Just make sure you run it once the rafalis-server is started :

```bash
./post-start-install.sh
```

For more information see [this link](https://gitlab.com/rafalis/rafalis-server#2-run-in-development-mode).

## Production Mode
In production, the database is launched via the main Rafalis `docker-compose.yml` file. You do not have to do anything here.

For more informations, refer to the main README in the Rafalis main repository.
