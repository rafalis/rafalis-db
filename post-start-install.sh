#!/bin/env bash

# Convert columns to text type instead of varchar type. Quarkus automatically sets string column types to varchar(255) wich is too short.

docker ps | grep -oh "\S*rafalis-db\S*" | while read container; do 
    echo "Proceeding for $container container..."
    RES=$(docker exec $container psql -U postgres -c "SELECT pg_typeof(main_title) FROM appconfig LIMIT 1;")
    if [[ -z "$RES" ]]; then
        echo "'$container''s database not initialized yet. First start the rafalis-server to create the schema, and re-run this script to update the column types."
    else 
        IS_VARCHAR=$(echo "$RES" | grep -I "varying")
        if [[ -z $IS_VARCHAR ]]; then
            echo "Columns already formatted !";
        else
            echo "'varchar' columns detected, converting them to 'text'";
            SETUP_SCRIPT_PATH=$(find . -name "setup-columns.sql" -print -quit)
            echo "Applying $SETUP_SCRIPT_PATH"
            cat $SETUP_SCRIPT_PATH | docker exec -i $container psql -U postgres
            echo "Columns properly formatted !"
        fi
    fi
done
